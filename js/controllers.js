angular.module('mantisApp.controllers', []).

  /* applications controller */
  controller('applicationsController', function($scope, ergastAPIservice) {
    $scope.nameFilter = null;
    $scope.applications = [];
    $scope.searchFilter = function (application) {
        var re = new RegExp($scope.nameFilter, 'i');
        return !$scope.nameFilter || re.test(application.name) || re.test(application.description);
    };

    ergastAPIservice.getApplications().success(function (response) {
        //Digging into the response to get the relevant data
        $scope.applications = response;
    });
  }).

  /* application controller */
  controller('applicationController', function($scope, $routeParams, ergastAPIservice) {
    $scope.id = $routeParams.id;
    $scope.application = null;

    ergastAPIservice.getApplicationDetails($scope.id).success(function (response) {
        $scope.application = response; 
    });

  });