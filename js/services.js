angular.module('marketApp.services', [])
  .factory('ergastAPIservice', function($http) {

    var ergastAPI = {};

    ergastAPI.getApplications = function() {
      return $http.get("http://localhost:8080/applications");
    }

    ergastAPI.getApplicationDetails = function(id) {
      return $http.get("http://localhost:8080/application");
    }

    return ergastAPI;
  });