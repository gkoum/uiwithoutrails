angular.module('mantisApp', [
  'mantisApp.services',
  'mantisApp.controllers',
  'ngRoute'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.
	when("/applications", {templateUrl: "partials/applications.html", controller: "applicationsController"}).
	when("/applications/:id", {templateUrl: "partials/application.html", controller: "applicationController"}).
	otherwise({redirectTo: '/applications'});
}]);