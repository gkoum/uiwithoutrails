'use strict';

/* Directives */


angular.module('mantisApp.directives', []).
  directive('mantisVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);
