(function() {
  var app = angular.module('marketPlace', []);

  app.controller('MarketController', [ '$http' , '$scope' , function($http , $scope){
    
    $http.get('http://localhost:8080/application').
        success(function(data) {
            $scope.application = data;
        });
  }]);

})();
